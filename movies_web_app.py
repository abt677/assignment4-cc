#!/usr/bin/env python3
import os
import time
import sys

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
import json


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movie(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, \
        title TEXT, director TEXT, actor TEXT, release_date TEXT, rating TEXT, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        # populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cnx.commit()
    print("Returning from populate_data")

def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    # cur.execute("SELECT greeting FROM message")
    # entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return 1

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    yr = request.form['year']
    ttl = request.form['title']
    drtr = request.form['director']
    actr = request.form['actor']
    rldt = request.form['release_date']
    rtng = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("INSERT INTO movie (year, title, director, actor, release_date, rating) \
            values ('" + yr + "', '" + ttl + "', '" + drtr + "', '" + actr + "', '" + rldt + "', '" + rtng + "')")
        cnx.commit()
        return "Movie " + ttl + " successfully inserted"
    except Exception as exp:
        return "Movie " + ttl + " could not be inserted - " + exp

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    yr = request.form['year']
    ttl = request.form['title']
    drtr = request.form['director']
    actr = request.form['actor']
    rldt = request.form['release_date']
    rtng = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("UPDATE movie SET \
            year = '" + yr + "', title = '" + ttl + "', director = '" + drtr + "', \
            actor = '" + actr + "', release_date = '" + rldt + "', rating = '" + rtng + "' \
            WHERE title = '" + ttl + "' ")
        cnx.commit()
        return "Movie " + ttl + " successfully updated"
    except Exception as exp:
        return "Movie " + ttl + " could not be updated - " + exp

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    ttl = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("DELETE FROM movie WHERE title = '" + ttl + "' ")
        cnx.commit()
        return "Movie " + ttl + " successfully deleted"
    except Exception as exp:
        return "Movie " + ttl + " could not be deleted - " + exp

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    actr = request.args.get('search_actor')

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movie WHERE actor = '" + actr + "' "" ")
        res = cur.fetchall()
        ret = ''
        for entry in res:
            ret += (entry[2] + ", " + entry[1] + ", " + entry[4] + '<br>')
        print(ret)
        return ret
    except Exception as exp:
        print(exp)
        return "No movies found for actor " + actr

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movie")
        res = cur.fetchall()
        ret = ''
        mx = -sys.maxsize
        for entry in res:
            if int(entry[6]) > mx:
                ret = (entry[2] + ", " + entry[1] + ", " + entry[4] + ", " + entry[3] + ", " + entry[6] + '<br>')
                mx = int(entry[6])
            elif int(entry[6]) == mx:
                ret += (entry[2] + ", " + entry[1] + ", " + entry[4] + ", " + entry[3] + ", " + entry[6] + '<br>')
                mx = int(entry[6])
        return ret
    except Exception as exp:
        print(exp)
        return "Error: " + exp

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movie")
        res = cur.fetchall()
        ret = ''
        mn = sys.maxsize
        for entry in res:
            if int(entry[6]) < mn:
                ret = (entry[2] + ", " + entry[1] + ", " + entry[4] + ", " + entry[3] + ", " + entry[6] + '<br>')
                mn = int(entry[6])
            elif int(entry[6]) == mn:
                ret += (entry[2] + ", " + entry[1] + ", " + entry[4] + ", " + entry[3] + ", " + entry[6] + '<br>')
                mn = int(entry[6])
        return ret
    except Exception as exp:
        print(exp)
        return "Error: " + exp

@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    query_data()
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
